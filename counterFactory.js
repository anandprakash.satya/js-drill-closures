// Return an object that has two methods called `increment` and `decrement`.
    // `increment` should increment a counter variable in closure scope and return it.
    // `decrement` should decrement the counter variable and return it.

    function counterFactory() {
        let count =50
        let object={
            increment:(count)=> count+1,
            decrement:(count)=> count-1
        }
        return [object.increment(count),object.decrement(count)]
    }
    module.exports = counterFactory