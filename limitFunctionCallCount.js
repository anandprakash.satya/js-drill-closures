// Should return a function that invokes `cb`.
    // The returned function should only allow `cb` to be invoked `n` times.
    // Returning null is acceptable if cb can't be returned

    function limitFunctionCallCount(cb, n) {
       
        let countOfCall = 0
        function invokeCB() {
            if (countOfCall<n){
                countOfCall++
                return cb()
            }else{
                return ("limit for calls exceeded")
            }
        }
        return invokeCB
        }

module.exports = limitFunctionCallCount