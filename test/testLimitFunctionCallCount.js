const limitFunctionCallCount = require('../limitFunctionCallCount')

let cb = () => {
   return ('Hello world!')
}
let i = limitFunctionCallCount(cb,3)

console.log(i())
console.log(i())
console.log(i())
console.log(i())
